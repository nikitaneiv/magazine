﻿using System;
using System.Collections.Generic;
using System.Text;

namespace magazine1
{
    class Edible : Product
    {

        public Edible(string Name, int Price) : base(Name, Price)
        {
            this._ediblenoedible = 1;
        }

        public override string ToString()
        {
            return $"Наименование товара: {_name} \nЦена: {_price}";
        }



    }
}
