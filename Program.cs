﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace magazine1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool cycle = true;
            while (cycle)
            {
                Console.Clear();
                Console.WriteLine($"{Magazine.NameOfMagazine} \n{Magazine.NameOfAdress}");
                Console.WriteLine("====================\n\n1.Добавить товар\n2.Удалить товар\n3.Показать список товаров\n4.Показать список товаров по цене\n5.Показать список товаров съедобных и не съедобных\n6.Показать страну производителя\n7.Показать дешевый и дорогой товар\n8.Выход\n");
                int num = Convert.ToInt32(Console.ReadLine());

                switch (num)
                {
                    case 1:
                        Magazine.AddProduct();
                        break;
                    case 2:
                        Magazine.RemoveProduct();
                        break;
                    case 3:
                        Magazine.ShowAllProduct();
                        break;
                    case 4:
                        Magazine.ShowAllProductPrice();
                        break;
                    case 5:
                        Magazine.ShowAllEdibleNoedible();
                        break;
                    case 6:
                        Magazine.ShowCountry();
                        break;
                    case 7:
                        Magazine.ShowLowHighPrice();
                        break;
                    case 8:
                        cycle = false;
                        break;


                }
            }
        }
    }
}


