﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace magazine1
{
    class Magazine : IShowCountry, ILowPrice, IHighPrice
    {
        private static string _nameofmagazine = "У дома";
        private static string _nameofadress = "ул. Болдина, дом 5";
        private static List<Product> productList = new List<Product>();

        public static string NameOfMagazine => _nameofmagazine;
        public static string NameOfAdress => _nameofadress;

        public static void AddProduct()
        {
            Console.Clear();
            Console.WriteLine("1.Введите наименование товара");
            string name = Console.ReadLine();
            Console.WriteLine("2.Введите цену товара");
            int price = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Товар съедобный?:\n 1. Да\n 2. Нет");
            int Select = Convert.ToInt32(Console.ReadLine());

            if (Select == 1)
            {
                Edible edible = new Edible(name, price);
                productList.Add(edible);
            }
            if (Select == 2)
            {
                Noedible noedible = new Noedible(name, price);
                productList.Add(noedible);
            }

            Console.Clear();
            Console.WriteLine($"Товар {name} добавлен!");

        }
        public static void RemoveProduct()
        {
            Console.Clear();
            Console.WriteLine("Введите наименование товара которое нужно удалить");
            string name = Console.ReadLine();
            for (int i = 0; i < productList.Count; i++)
            {
                if (name == productList[i].Name)
                {
                    string productname = productList[i].Name;
                    productList.Remove(productList[i]);
                    Console.WriteLine($"Товар {productname} удалён!");
                    Console.ReadLine();
                }
            }
        }
        public static void ShowAllProduct()
        {
            Console.Clear();
            if (productList.Count == 0)
            {
                Console.WriteLine($"В магазине нет товаров");
            }
            else
            {
                Console.WriteLine($"Всего товаров в магазине {productList.Count}\n====================\n");
                productList.Sort((left, right) => left.Name.CompareTo(right.Name));
                foreach (Product sortedproductList in productList)
                {
                    Console.WriteLine($"Наименование товара: {sortedproductList.Name}\nЦена: {sortedproductList.Price}\n====================\n");
                }
            }
            Console.WriteLine("Для продолжения нажмите любую кнопку");
            Console.ReadLine();
        }
        public static void ShowAllProductPrice()
        {
            Console.Clear();
            var sortedproductList = productList.OrderBy(p => p.Price);
            foreach (var p in sortedproductList)
            {
                Console.WriteLine($"Наименование товара: {p.Name}\nЦена: {p.Price}\n====================\n");
            }
            Console.WriteLine("Для продолжения нажмите любую кнопку");
            Console.ReadLine();
        }
        public static void ShowAllEdibleNoedible()
        {
            Console.Clear();
            Console.WriteLine("Показать товар:\n 1. Съедобный\n 2. Не съедобный");
            int Select = Convert.ToInt32(Console.ReadLine());

            if (Select == 1)
            {
                foreach (Product product in productList)
                {
                    if (product.EdibleNoedible == 1)
                        Console.WriteLine(product);
                }

            }
            if (Select == 2)
            {
                foreach (Product product in productList)
                {
                    if (product.EdibleNoedible == 2)
                        Console.WriteLine(product);
                }
            }

            Console.WriteLine("Для продолжения нажмите любую кнопку");
            Console.ReadLine();
        }
        public void Country(string country) => Console.WriteLine($"Страна изготовитель:{country} ");
        public void Low(string low) => Console.WriteLine($"Товар стоит меньше 50 рублей,{low} ");
        public void High(string high) => Console.WriteLine($"Товар стоит дороже 50 рублей,{high} ");
        public static void ShowCountry()
        {
            Console.Clear();
            Console.WriteLine("Введите наименование товара");
            string name = Console.ReadLine();
            for (int i = 0; i < productList.Count; i++)
            {
                if (name == productList[i].Name)
                {
                    productList[i].Country("Беларусь");
                }
            }
            Console.WriteLine("Для продолжения нажмите любую кнопку");
            Console.ReadLine();
        }
        public static void ShowLowHighPrice()
        {
            Console.Clear();
            Console.WriteLine("Введите наименование товара ");
            string name = Console.ReadLine();
            for (int i = 0; i < productList.Count; i++)
            {
                if (name == productList[i].Name)
                {
                    if (productList[i].Price < 50)
                    {
                        productList[i].Low("товар не дорогой");
                    }
                    else
                    {
                        productList[i].High("товар дорогой");
                    }
                }
            }
            Console.WriteLine("Для продолжения нажмите любую кнопку");
            Console.ReadLine();
        }
    }
}
