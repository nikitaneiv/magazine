﻿using System;
using System.Collections.Generic;
using System.Text;

namespace magazine1
{
    class Noedible : Product
    {

        public Noedible(string Name, int Price) : base(Name, Price)
        {
            this._ediblenoedible = 2;
        }
        public override string ToString()
        {
            return $"Наименование товара: {_name} \nЦена: {_price} ";
        }


    }
}
