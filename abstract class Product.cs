﻿using System;
using System.Collections.Generic;
using System.Text;

namespace magazine1
{
    abstract class Product : IShowCountry, ILowPrice, IHighPrice
    {
        protected string _name;
        protected int _price;
        protected int _ediblenoedible;

        public string Name => _name;
        public int Price => _price;
        public int EdibleNoedible => _ediblenoedible;



        public Product(string Name, int Price)
        {
            this._name = Name;
            this._price = Price;


        }

        public void High(string high) => Console.WriteLine($"Товар стоит дороже 50 рублей, {high} ");
        public void Low(string low) => Console.WriteLine($"Товар стоит меньше 50 рублей, {low} ");
        public void Country(string country) => Console.WriteLine($"Страна изготовитель: {country} ");

    }
}
